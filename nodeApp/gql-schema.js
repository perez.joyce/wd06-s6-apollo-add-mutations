//1) declare dependencies
const graphql = require('graphql');
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLBoolean,
  GraphQLList,
  GraphQLSchema,
  GraphQLNonNull
} = graphql;
const qraphqlISODate = require('graphql-iso-date');
const { GraphQLDateTime } = qraphqlISODate;

//2) declare mongoose models
const Member = require('./models/member');
const Task = require('./models/task');
const Team = require('./models/team');

//3) create types and update GraphQL classes in #1
//to define and model our records from MongoDB
const TeamType = new GraphQLObjectType({
  name: 'Team',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
    tasks: {
      type: new GraphQLList(TaskType),
      resolve: (parent, args) => {
        return Task.find({ teamId: parent.id });
      }
    }
  })
});

//TASK TYPE
const TaskType = new GraphQLObjectType({
  name: 'Task',
  fields: () => ({
    id: { type: GraphQLID },
    description: { type: GraphQLString },
    teamId: { type: GraphQLString },
    isCompleted: { type: GraphQLBoolean },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
    team: {
      type: TeamType,
      resolve: (parent,args) => {
        return Team.findById(parent.teamId);
      }
    }
  })
});

//MEMBER TYPE
const MemberType = new GraphQLObjectType({
  name: 'Member',
  fields: () => ({
    id: { type: GraphQLID },
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    position: { type: GraphQLString },
    teamId: { type: GraphQLString },
    createdAt: { type: GraphQLDateTime },
    updatedAt: { type: GraphQLDateTime },
    team: {
      type: TeamType,
      resolve: (parent,args) => {
        return Team.findById(parent.teamId);
      }
    }
  })
});

//4) create entry point (i..e., index, show)
/*
	field - defines the possible queries
*/
const RootQuery = new GraphQLObjectType({
  name: 'Query',
  fields: {
    teams: {
      type: new GraphQLList(TeamType),
      resolve: (parent, args) => {
        return Team.find({});
      }
    },
    team: {
      type: TeamType,
      args: {
        id: { type: GraphQLID }
      },
      resolve: (parent, args) => {
        return Team.findById(args.id);
      }
    },
    tasks: {
      type: new GraphQLList(TaskType),
      resolve: (parent, args) => {
        return Task.find({});
      }
    },
    task: {
      type: TaskType,
      args: {
        id: { type: GraphQLID }
      },
      resolve: (parent, args) => {
        return Task.findById(args.id);
      }
    },
    members: {
      type: new GraphQLList(MemberType),
      resolve: (parent, args) => {
        return Member.find({});
      }
    },
    member: {
      type: MemberType,
      args: {
        id: { type: GraphQLID }
      },
      resolve: (parent, args) => {
        return Member.findById(args.id);
      }
    }
  }
});

//6) create mutations (i.e., store, update, destroy, etc)
const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    storeTeam: {
      type: TeamType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => {
        let team = new Team({
          name: args.name
        });

        return team.save();
      }
    },
    destroyTeam: {
      type: TeamType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) }
      },
      resolve: (parent, args) => {
        let condition = { _id: args.id };
        return Team.findByIdAndRemove(condition);
      }
    },
    updateTask: {
      type: TaskType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        description: { type: GraphQLString },
        teamId: { type: GraphQLString },
        isCompleted: { type: GraphQLBoolean }
      },
      resolve: (parent, args) => {
        let condition = { _id: args.id };
        let updates = {
          description: args.description,
          teamId: args.teamId,
          isCompleted: args.isCompleted
        };

        return Task.findOneAndUpdate(condition, updates, task => {
          return task;
        });
      }
    },
    storeMember: {
      type: MemberType,
      args: {
        firstName: { type: new GraphQLNonNull(GraphQLString) },
        lastName: { type: new GraphQLNonNull(GraphQLString) },
        position: { type: new GraphQLNonNull(GraphQLString) },
        teamId: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => {
        let member = new Member(args);
        return member.save();
      }
    },
    updateMember: {
      type: MemberType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        position: { type: GraphQLString }
      },
      resolve: (parent, args) => {
        let condition = { _id: args.id };
        return Member.findOneAndUpdate(condition, args, { new: true });
      }
    },
    destroyMember: {
      type: MemberType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) }
      },
      resolve: (parent, args) => {
        let condition = { _id: args.id };
        return Member.findByIdAndRemove(condition);
      }
    },
    storeTask: {
      type: TaskType,
      args: {
        description: { type: new GraphQLNonNull(GraphQLString) },
        teamId: { type: new GraphQLNonNull(GraphQLString) },
        isCompleted: { type: GraphQLBoolean }
      },
      resolve: (parent, args) => {
        return Team.exists({ _id: args.teamId })
          .then(() => {
            let task = new Task(args);
            console.log('saved');
            return task.save();
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  }
});

//5) export GraphQL schema
module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
