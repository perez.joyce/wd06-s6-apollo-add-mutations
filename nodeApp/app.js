//1)declare dependencies
const express = require("express");
const app = express();
const graphqlHTTP = require("express-graphql");
const graphqlSchema = require("./gql-schema");
const cors = require("cors");

//3)connect to db
const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/merng_tracker', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", ()=> {
	console.log("Now connected to local MongoDB Server");
})

//4) Middleware
app.use(cors());
app.use(
  '/graphql',
  graphqlHTTP({
    schema: graphqlSchema,
    graphiql: true,
  }),
);

// http://localhost:4007/graphql

//2)initialize server
app.listen(4007, ()=> {
	console.log("Now listening for requests on port 4010 :)");
});
