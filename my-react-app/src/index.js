import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import MemberPage from './pages/MemberPage';
import TeamPage from './pages/TeamPage';
import TaskPage from './pages/TaskPage';
import NotFound from './components/NotFound';
import AppNavbar from './components/AppNavbar';

const root = document.querySelector("#root");
const client = new ApolloClient({ uri: 'http://localhost:4007/graphql'})

const pageComponent = (
		<ApolloProvider client={client}>
			<BrowserRouter>
				<AppNavbar />
				<Switch>
					<Route exact path="/members" component={MemberPage} />
					<Route exact path="/teams" component={TeamPage}/>
					<Route exact path="/tasks" component={TaskPage}/>
					<Route component={NotFound}/>
				</Switch>
			</BrowserRouter>
		</ApolloProvider>
	);

ReactDOM.render(pageComponent, root);