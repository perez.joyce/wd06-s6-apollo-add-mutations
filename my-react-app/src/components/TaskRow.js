import React, { Component } from 'react';

const TaskRow = (props) => {

		const task = props.task;
		return (
			<tr key={task.id}>
				<td>{task.description}</td>
				<td>{task.teamId}</td>
				<td>Sample</td>
			</tr>
		)
}

export default TaskRow;