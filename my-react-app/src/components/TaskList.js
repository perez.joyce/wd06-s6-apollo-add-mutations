import React, { useState, useEffect } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Card } from 'react-bulma-components';

import TaskRow from './TaskRow';

const TaskList = (props) => {

	// console.log(props);
	let rows = "";
	if(props.tasks.length === 0) {
		rows = (
				<tr>
					<td colSpan="4">
						<em>No tasks found.</em>
					</td>
				</tr>
			)
	} else {
		rows = props.tasks.map(task => {
				return <TaskRow task={task} key={task.id}/>
			})
	}

		return (
			<table className="table is-fullwidth is-bordered is-striped is-hoverable">
				<thead>
					<tr>
						<th>Description</th>
						<th>Team</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{rows}
				</tbody>
			</table>
		)
}

export default TaskList;