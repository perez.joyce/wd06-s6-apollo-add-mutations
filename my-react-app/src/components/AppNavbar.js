import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { 
	Navbar
} from 'react-bulma-components';
import { Link } from 'react-router-dom';

class AppNavbar extends Component {
	render()
	{
		return (
			<Navbar className="is-black">
				<Navbar.Brand>
					<Navbar.Item>
						<strong>MERNG Tracker</strong>
					</Navbar.Item>
					<Navbar.Burger/>
				</Navbar.Brand>
				<Navbar.Menu>
					<Navbar.Container>
						
							<Link className="has-text-white navbar-item" to="/members">
								Members
							</Link>
							<Link className="has-text-white navbar-item" to="/teams">
								Teams
							</Link>
							<Link className="has-text-white navbar-item" to="/tasks">
								Tasks
							</Link>
					</Navbar.Container>
				</Navbar.Menu>
			</Navbar>
		)
	}
}

export default AppNavbar;