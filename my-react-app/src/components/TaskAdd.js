import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Card } from 'react-bulma-components';


const TaskAdd = (props) => {
	// console.log(props);
	const [description, setDescription] = useState("");
	const [teamId, setTeamId] = useState(undefined);
	// console.log(description); // ""
	// console.log(teamId);

	const addTask = e => {
		e.preventDefault();

		const newTask = {
			description: description,
			teamId: teamId
		}

		setDescription('');
		setTeamId(undefined);

		props.addTask(newTask);
		//Swal

	}


	return (
		<Card>
			<Card.Header>
				<Card.Header.Title>Add Task</Card.Header.Title>
			</Card.Header>
			<Card.Content>
				<form onSubmit={addTask}>
					<div className="field">
					  <label className="label">Description</label>
					  <div className="control">
					    <input 
					    className="input" 
					    type="text"
					    required
					    value={description}
					    onChange={e => 
					    	{
					    		setDescription(e.target.value);
					    		console.log(description);
					    	}
					    }
					    />
					  </div>
					</div>
					<div className="field">
					  <label className="label">Teams</label>
					  <div className="control">
					    <div className="select is-fullwidth">
					      <select
					      	required
					      	value={teamId}
					      	onChange={e=> setTeamId(e.target.value)}
					      >
					        <option>Select Team</option>
					        <option>With options</option>
					      </select>
					    </div>
					  </div>
					</div>
					<br/>
					<div className="field">
					  <div className="control">
					    <button className="button is-link is-success">Add</button>
					  </div>
					 </div>
				</form>
			</Card.Content>
		</Card>
	)
}

export default TaskAdd;