import React from 'react';

const MemberRow = (props) => {
		return (
			<tr key={ props.member.id }>
				<td>{ props.member.firstName + " " + props.member.lastName }</td>
				<td>{ props.member.position }</td>
				<td>{ props.member.team.name }</td>
				<td>Sample</td>
			</tr>
		)
}

export default MemberRow;