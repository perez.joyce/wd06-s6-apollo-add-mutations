import React, { useState } from 'react';
//10)importing sweetalert2
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { 
	Card 
} from 'react-bulma-components';

import { graphql } from 'react-apollo';
import { getTeamsQuery } from '../graphql/queries';

const MemberAdd = (props) => {
		/*
			4.a. Add state to child component. 
      		- state variables will be bound to individual input fields
		*/
		const [firstName, setFirstName] = useState("");
		const [lastName, setLastName] = useState("");
		const [position, setPosition] = useState("");
		const [teamId, setTeamId] = useState(undefined);

		/*
			   4.b. create addMmeber function 
		      - prevent defafult behavior of form
		      - reset values of input fields
		      - call props from parent component to create a new document
		*/
		const addMember = (e) => {
			e.preventDefault();

			const newMember = {
				firstName: firstName,
				lastName: lastName,
				position: position,
				teamId: teamId
			}

			setFirstName("");
			setLastName("");
			setPosition("");
			setTeamId("DEFAULT");

			props.addMember(newMember);
			//11) fire sweetAlert
			Swal.fire({
				title: "Member Added",
				text: "Member has been added!",
				type: "success"
			})
		}

		//populate Team option
		const populateTeamOptions = () => {
			let data = props.data;
			if(data.loading) {
				return <option>Loading teams...</option>
			} else {
				return data.teams.map(team => {
					return (<option key={team.id} value={team.id}>
						{team.name}
					</option>)
				})
			}
		}

		return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Add Member
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
			{/*
				  5. Employ two-way binding
			*/}
					<form onSubmit={addMember}>
						<div className="field">
						  <label className="label">First Name</label>
						  <div className="control">
						    <input 
						    className="input" 
						    type="text"
						    required
						    value={firstName}
						    onChange={e => setFirstName(e.target.value)}
						    />
						  </div>
						</div>
						<div className="field">
						  <label className="label">Last Name</label>
						  <div className="control">
						    <input 
						    className="input" 
						    type="text"
						    required
						    value={lastName}
						    onChange={e => setLastName(e.target.value)}
						    />
						  </div>
						</div>
						<div className="field">
						  <label className="label">Position Name</label>
						  <div className="control">
						    <input 
						    className="input" 
						    type="text"
						    required
						    value={position}
						    onChange={e => setPosition(e.target.value)}
						    />
						  </div>
						</div>
						<div className="field">
						  <label className="label">Team</label>
						  <div className="control">
						    <div className="select is-fullwidth">
						      <select 
						      	defaultValue={"DEFAULT"}
						      	required
						      	value={teamId}
						      	onChange={e => setTeamId(e.target.value)}
						      >
						        <option value="DEFAULT" disabled>Select team</option>
						        { populateTeamOptions() }
						      </select>
						    </div>
						  </div>
						</div>
						<br/>
						<div className="field">
						  <div className="control">
						    <button type="submit" className="button is-link is-success">Add</button>
						  </div>
						</div>
					</form>
				</Card.Content>
			</Card>
		)
}

export default graphql(getTeamsQuery)(MemberAdd);