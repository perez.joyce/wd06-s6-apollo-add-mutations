import React from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns, Card } from 'react-bulma-components';

class TeamPage extends React.Component {
	render()
	{
		const sectionStyle = {
			paddingTop: "3em",
			paddingBottom: "3em"
		}

		return (
			<Section size="medium" style={sectionStyle}>
				<Heading>Teams</Heading>
				<Columns>
					<Columns.Column className="is-4">
						<Card>
							<Card.Header>
								<Card.Header.Title>Add Team</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<form>
									<div className="field">
									  <label className="label">Name</label>
									  <div className="control">
									    <input className="input" type="text"/>
									  </div>
									</div>
									<br/>
									<div class="field">
									  <div class="control">
									    <button class="button is-link is-success">Add</button>
									  </div>
									 </div>
								</form>
							</Card.Content>
						</Card>
					</Columns.Column>
					<Columns.Column>
						<Card.Header>
								<Card.Header.Title>Team List</Card.Header.Title>
							</Card.Header>
							<Card.Content>
								<table className="table is-fullwidth is-bordered is-striped is-hoverable">
									<thead>
										<tr>
											<th>Team Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Sample</td>
											<td>Sample</td>
										</tr>
									</tbody>
								</table>
							</Card.Content>
					</Columns.Column>
				</Columns>
			</Section>
		)
	}
}

export default TeamPage;