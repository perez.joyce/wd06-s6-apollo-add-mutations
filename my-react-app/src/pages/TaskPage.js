import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Section, Heading, Columns, Card } from 'react-bulma-components';

import TaskAdd from '../components/TaskAdd';
import TaskList from '../components/TaskList';

const TaskPage = (props) => {

	// console.log(useState()); //returns two elements: undefined, func

	// console.log(useState('hello')); 

	const [tasks, setTasks] = useState([]);

	const addTask = (newTask) => {
		setTasks([...tasks, newTask]);
	}

	const sectionStyle = {
		paddingTop: "3em",
		paddingBottom: "3em"
	}

	return (
		<Section size="medium" style={sectionStyle}>
			<Heading>Tasks</Heading>
			<Columns>
				<Columns.Column className="is-4">
					<TaskAdd addTask={addTask}/>
				</Columns.Column>
				<Columns.Column>
					<Card>
						<Card.Header>
							<Card.Header.Title>Task List</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<TaskList tasks={tasks}/>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Section>
	)
}

export default TaskPage;