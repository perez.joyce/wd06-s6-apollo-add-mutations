import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { 
	Section,
	Heading,
	Columns
} from 'react-bulma-components';
import MemberAdd from '../components/MemberAdd';
import MemberList from '../components/MemberList';

import compose from 'lodash.flowright';
import { graphql } from 'react-apollo';

import { getMembersQuery } from '../graphql/queries';
import { storeMemberMutation } from '../graphql/mutations';

const MemberPage = (props) => {
	console.log(props);

	const [members, setMembers] = useState([]);
	const data = props.getMembersQuery;

	const addMember = (newMember) => {
		setMembers([...members, newMember]);
	}

	const sectionStyle = {
		paddingTop: "3em",
		paddingBottom: '3em'
	}

	return (
		<Section size="medium" style={ sectionStyle }>
			<Heading>Members</Heading>
			<Columns>
				<Columns.Column size={4}>
					{/*   3. Pass the new function as prop to child component */}
					<MemberAdd addMember={addMember}/>
					}
				</Columns.Column>
				<Columns.Column>
				 {/*6) pass props */}
					<MemberList members={data.members}/>
				</Columns.Column>
			</Columns>
		</Section>
	)
}

export default compose(
	graphql(getMembersQuery, { name: 'getMembersQuery'}),
	graphql(storeMemberMutation, { name: 'storeMemberMutation'}),
)(MemberPage);