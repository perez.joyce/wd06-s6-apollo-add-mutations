import {gql} from 'apollo-boost';


const storeMemberMutation = gql`
mutation(
	$firstName: String!
	$lastName: String!
	$position: String!
	$teamId: String!
)
{
	storeMember(
		firstName: $firstName
		lastName: $lastName
		position: $position
		teamId: $teamId	

	){
		firstName
		lastName
	}
}
`
export {storeMemberMutation};


const storeTeamMutation = gql`
mutation(
	$name: String!
)
{
	storeTeam(
		name: $name
	){
		name
	}
}
`
export{storeTeamMutation}


const storeTaskMutation = gql`
mutation(
	$description: String!
	$teamId: String!
)
{
	storeTask(
		description: $description
		teamId: $teamId
	){
		description
		teamId
	}
}
`
export{storeTaskMutation}