import {gql} from 'apollo-boost';

const getMembersQuery = gql
`{
	members {
		id
		firstName
		lastName
		position
		teamId
		team{
			name
		}

	}

}`


const getTeamsQuery = gql
`{
	teams {
		id
		name
	}

}`


const getTasksQuery = gql
`{
	tasks {
		id
		description
		teamId
		team{
			name
		}
	}

}`

export {getMembersQuery, getTeamsQuery, getTasksQuery };